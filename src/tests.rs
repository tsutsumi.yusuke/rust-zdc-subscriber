use super::{ZDCMetric, ZDCMetricParser};

#[test]
fn test_verify_json_parsing() {
    let bytes = String::from("{\"pth\": \"foo\", \"cnt\": \"10\"}").into_bytes();
    let mut parser = ZDCMetricParser::new();
    let metric = parser.from_json_bytes(&bytes);
    assert!(&metric.path == b"foo",
            format!("{}", String::from_utf8_lossy(&metric.path)));
    assert!(&metric.metric_type == b"c");
    assert!(&metric.metric_value == b"10");
}

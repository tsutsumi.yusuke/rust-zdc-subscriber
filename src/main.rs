#![feature(lookup_host)]
extern crate zmq;
extern crate serde_json;
use std::{env, str, fmt, io, net};
use std::net::{UdpSocket, SocketAddr};
use serde_json::{Value};

#[cfg(test)]
mod tests;

// OPTIMIZATION TODO
// reuse a buffer for receving messages?
// custom json parser

// static QUOTE: u8 = '\"' as u8;
// static DOT: u8 = '.' as u8;

fn benchmark() {
    let bytes = b"{\"pth\": \"foo\", \"cnt\": \"10\"}";
    let mut parser = ZDCMetricParser::new();
    let mut metric = ZDCMetric::new();
    let mut i = 0;
    while i < 1000000 {
        parser.populate(bytes, &mut metric);
        i += 1;
    }
}

fn bechmark_serde() {
    let body = "{\"pth\": \"foo\", \"cnt\": \"10\"}";
    for i in 0..100000000 {
        let deserialized: Value = serde_json::from_str(&body).unwrap();
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let ref broker_url = args[1];
    let ref statsite_host = args[2];
    let ref statsite_port = args[3];
    println!("looking up port...");
    match net::lookup_host(&statsite_host).unwrap().next() {
        None => { println!("host {} not found", &statsite_host);},
        Some(host_addr) => {
            let mut statsite_addr = host_addr.clone();
            statsite_addr.set_port(u16::from_str_radix(&statsite_port, 10).unwrap());
            println!("{}", broker_url);
            println!("{}", statsite_addr);

            let ctx = zmq::Context::new();
            let statsd = StatsDClient::new(statsite_addr).unwrap();
            // the parser was created to
            // pre-allocate the key buffer.
            // SAVED: 3ns per json processed.
            let mut parser = ZDCMetricParser::new();
            // allocating and reusing a metric object (vs allocating every time) increased speed
            // SAVED: 30ns per json processed.
            let mut metric = ZDCMetric::new();
            // TODO OPTIMIZATION: pre-allocating a buffer to keep using for zmq.
            // let mut buffer: [u8; 10000] = [0; 10000];
            let socket = ctx.socket(zmq::SUB).unwrap();
            socket.connect(broker_url).unwrap();
            socket.set_subscribe(&String::from("zdc").into_bytes()).unwrap();
            loop {
                let multipart = socket.recv_multipart(0).unwrap();
                parser.populate(&multipart[3], &mut metric);
                match statsd.send_zdc_metric(&metric) {
                    Ok(_) => { }// println!("{}", metric);},
                    Err(e) => {}  //println!("error: {}", e);}
                };
            }
        }
    }
}

struct StatsDClient {
    _socket: UdpSocket,
    _addr: SocketAddr
}

impl StatsDClient {

    #[inline]
    pub fn new(addr: SocketAddr) -> io::Result<StatsDClient> {
        let socket = try!(UdpSocket::bind("127.0.0.1:0"));
        return Ok(StatsDClient {
            _socket: socket,
            _addr: addr
        })
    }

    #[inline]
    pub fn send(&self, path: &[u8], metric_value: &[u8], metric_type: &[u8]) -> io::Result<usize> {
        let size = path.len() + metric_value.len() + metric_type.len() + 2;
        let mut buf: Vec<u8> = Vec::with_capacity(size);
        buf.extend(path);
        buf.push(b':'); // : in ascii
        buf.extend(metric_value);
        buf.push(b'|'); // | in ascii
        buf.extend(metric_type);
        // try!(self._socket.send_to(&buf, &self._path))
        self._socket.send_to(&buf, self._addr)
    }

    #[inline]
    pub fn send_zdc_metric(&self, metric: &ZDCMetric) -> io::Result<usize>{
        if metric.path.len() > 11 && &(metric.path[0..11]) == b"zdc.metrics" {
            try!(self._convert_to_trend_and_send(metric));
        }
        self.send(&metric.path, &metric.metric_value, &metric.metric_type)
    }

    #[inline]
    pub fn _convert_to_trend_and_send(&self, metric: &ZDCMetric) -> io::Result<usize> {
        let mut new_path: Vec<u8> = vec![];
        new_path.extend(b"zdc.trends");
        let mut num_dots = 0;
        for i in 11..metric.path.len() {
            let c = metric.path[i];
            match c {
                b'.' => {
                    num_dots += 1;
                    if num_dots != 3 {
                        new_path.push(b'.');
                    }
                },
                _ => {
                    if !(num_dots == 3) {
                        new_path.push(c);
                    }
                }
            }
        }
        self.send(&new_path, &metric.metric_value, &metric.metric_type)
    }
}

struct ZDCMetric {
    path: Vec<u8>,
    metric_type: &'static[u8],
    metric_value: Vec<u8>
}

impl fmt::Display for ZDCMetric {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}, {}, {}",
               str::from_utf8(&self.path).unwrap(),
               str::from_utf8(&self.metric_type).unwrap(),
               str::from_utf8(&self.metric_value).unwrap()
               )
    }
}

enum LiteralType {
    Path,
    Value,
    Key,
    None
}

struct ZDCMetricParser {
    key: Vec<u8>
}

impl ZDCMetricParser {
    pub fn new() -> ZDCMetricParser {
        return ZDCMetricParser {
            key: Vec::with_capacity(100),
        }
    }

    #[inline]
    pub fn from_json_bytes(&mut self, body: &[u8]) -> ZDCMetric {
        let mut metric = ZDCMetric::new();
        self.populate(body, &mut metric);
        metric
    }

    // building a custom json parser results in ~250ns increase per json document
    // (testing the standard json parsing with rust resulted in ~400ns per json document
    // parsed). Initial implementation pass was ~150ns for a trivial payload, optimizied down to 79ns.
    #[inline]
    pub fn populate(&mut self, body: &[u8], metric: &mut ZDCMetric) {
        metric.path.clear();
        metric.metric_value.clear();
        let mut literal_start = false;
        let mut literal_type: LiteralType = LiteralType::Key;
        let mut i = 0;
        // switching from iterator to raw for loop was faster, though it shouldn't be.
        // SAVED: 2ns per json processed.
        while i < body.len() {
            let b = body[i];
            match b {
                b'\"' => {
                    literal_start = !literal_start;
                    if !literal_start {
                        literal_type = match literal_type {
                            LiteralType::Key => {
                                match &*self.key {
                                    b"cnt" => {
                                        metric.metric_type = b"c";
                                        LiteralType::Value
                                    },
                                    b"lat" => {
                                        metric.metric_type = b"ms";
                                        LiteralType::Value
                                    },
                                    b"lms" => {
                                        metric.metric_type = b"ms";
                                        LiteralType::Value
                                    },
                                    b"gau" => {
                                        metric.metric_type = b"g";
                                        LiteralType::Value
                                    },
                                    b"pth" => LiteralType::Path,
                                    _ => LiteralType::None
                                }
                            },
                            _ => {
                                self.key.clear();
                                LiteralType::Key
                            }
                        }
                    }
                }
                b'1' | b'2' | b'3' | b'4' | b'5' | b'6' | b'7'| b'8' | b'9'| b'0' => {
                    match literal_type {
                        LiteralType::Value => {
                            metric.metric_value.push(b);
                        },
                        _ => {}
                    }
                },
                _ => {
                    if literal_start {
                        match literal_type {
                            LiteralType::Path => {metric.path.push(b);},
                            LiteralType::Key => {self.key.push(b);},
                            _ => {}
                        }
                    }
                }
            }
            i += 1;
        }
        // println!("{}", String::from_utf8_lossy(&path));
        // println!("{}", String::from_utf8_lossy(&metric_type));
        // println!("{}", String::from_utf8_lossy(&metric_value));
    }
}

impl ZDCMetric {

    pub fn new() -> ZDCMetric {
        return ZDCMetric {
            path: Vec::with_capacity(100),
            metric_type: b"g",
            metric_value: Vec::with_capacity(100)
        }
    }
}
